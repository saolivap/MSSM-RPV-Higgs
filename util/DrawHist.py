import ROOT 
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/Phenomenology/HTM/util/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/Phenomenology/HTM/util/AtlasStyle/AtlasLabels.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/Phenomenology/HTM/util/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad

#Flags
doLogX = False
doLogY = True

basepath = '/home/saolivap/WorkArea/Phenomenology/HTM/build/'
tree = 'trip'
plots = ['mh_v1']
hists = ['mh1', 'mh2', 'mh3', 'mA', 'mch', 'mdelta', 'dww', 'dhw', 'dee', 'duu', 'dtt', 'deu', 'det', 'dut']
params = ['v1', 'v2', 'v3']
plots = ['higgsmass', 'decays']
 
sample = basepath + 'trip.root'

outfile = ROOT.TFile('/home/saolivap/WorkArea/Phenomenology/HTM/util/hist.root','RECREATE')

# CREATING HISTOGRAMS
h = {}
for param in params:
    h[param] = {}

    for hist in hists:
        h[param][hist] = {}
        
        n = 'h_%s_%s' % (param, hist)
        if hist == 'mh1' or hist == 'mh2' or hist == 'mh3' or hist == 'mA' or hist == 'mch' or hist == 'mdelta':
            h[param][hist] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (hist, param), 200, 0, 5000, 200, 0, 5000) # was 5000 instead of 0.1
            h[param][hist].SetDirectory(outfile)
        elif hist == 'dww' or hist == 'dhw' or hist == 'dee' or hist == 'duu' or hist == 'dtt' or hist == 'deu' or hist == 'det' or hist == 'dtt':
            h[param][hist] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (hist, param), 200, 0, 5000, 200, 0, 0.1) # was 5000 instead of 0.1
            h[param][hist].SetDirectory(outfile)

#FOR LEGEND
g = {}
for hist in hists:
    g[hist] = {}
    n = 'h_%s' % (hist)
    g[hist] = ROOT.TH1F(n, n + "; %s [GeV]; Events" % (hist), 200, 0, 5000)
    g[hist].SetDirectory(outfile)

    
# FILLING
for param in params:
    print 'processing parameter: %s' % param
    
    for hist in hists:
        print 'processing histogram: %s' % hist
    
        f = ROOT.TFile.Open(sample)
        t = f.Get(tree)
        
        if hist == 'mh1' or hist == 'mh2' or hist == 'mh3' or hist == 'mA' or hist == 'mch' or hist == 'mdelta':
            n = 'htmp(200,0,5000,200,0,5000)' # was 5000 instead of 0.1
            if t.Draw('%s:%s>>%s' % (hist, param, n),
                      '', 
                      'goff'
            ):
                h[param][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)

#        elif hist == 'dww' or hist == 'dhw' or hist == 'dee' or hist == 'duu' or hist == 'dtt' or hist == 'deu' or hist == 'det' or hist == 'dtt':
#            n = 'htmp(200,0,5000,200,0,0.1)' # was 5000 instead of 0.1
#            if t.Draw('%s:%s>>%s' % (hist, param, n),
#                      '', 
#                      'goff'
#            ):
#                h[param][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)

del f

# PLOTTING
#for hist in hists:
    h['v1'][hist].GetXaxis().SetTitle('v_{1} [GeV]')
    h['v2'][hist].GetXaxis().SetTitle('v_{2} [GeV]')
    h['v3'][hist].GetXaxis().SetTitle('v_{3} [GeV]')
    h['v1'][hist].GetXaxis().SetRangeUser(10, 1000)
    h['v3'][hist].GetXaxis().SetRangeUser(0.00001, 0.35)

for param in params:

#    h[param]['mh3'].GetYaxis().SetRangeUser(10,1000)
    h[param]['mh3'].GetYaxis().SetTitle('m_{h} [GeV]')
#    h[param]['mA'].GetYaxis().SetTitle('m_{A} [GeV]')

    h[param]['mh1'].SetMarkerColor(ROOT.kRed)
    h[param]['mh2'].SetMarkerColor(ROOT.kGreen)
    h[param]['mh3'].SetMarkerColor(ROOT.kBlue)
    h[param]['mA'].SetMarkerColor(ROOT.kMagenta)
    h[param]['mch'].SetMarkerColor(ROOT.kCyan)
    h[param]['mdelta'].SetMarkerColor(ROOT.kPink)    
    #for legend
    g['mh1'].SetMarkerColor(ROOT.kRed)
    g['mh2'].SetMarkerColor(ROOT.kGreen)
    g['mh3'].SetMarkerColor(ROOT.kBlue)
    g['mA'].SetMarkerColor(ROOT.kMagenta)
    g['mch'].SetMarkerColor(ROOT.kCyan)
    g['mdelta'].SetMarkerColor(ROOT.kPink)
    
    for hist in hists:
        h[param][hist].SetMarkerSize(1)
        h[param][hist].SetMarkerStyle(ROOT.kDot)
        #for legend
        g[hist].SetMarkerSize(1)
        g[hist].SetMarkerStyle(ROOT.kFullCircle)

    tex = ROOT.TLatex(0.2, 0.88, "Variation of all parameters")
    tex.SetName('Test2')
    tex.SetNDC(True)
    tex.SetTextSize(0.04)

    #legend configuration
    xmin = 0.20
    xmax = 0.35
    ymin = 0.6
    ymax = 0.85

    legend_mass = ROOT.TLegend(xmin, ymin, xmax, ymax) 
    legend_mass.SetFillColor(0)
    legend_mass.SetBorderSize(0)
    legend_mass.SetTextFont(42)
    legend_mass.SetTextSize(0.05) 
    legend_mass.SetName('Masses')
    legend_mass.SetShadowColor(0)
    
    legend_mass.AddEntry(g['mh1'],'m_{h1}', 'p')
    legend_mass.AddEntry(g['mh2'],'m_{h2}', 'p')
    legend_mass.AddEntry(g['mh3'],'m_{h3}', 'p')
    legend_mass.AddEntry(g['mA'],'m_{A}', 'p')
    legend_mass.AddEntry(g['mch'],'m_{H#pm}', 'p')
    legend_mass.AddEntry(g['mdelta'],'m_{#Delta#pm#pm}', 'p')

    for plot in plots:
        print 'processing plot: %s' % plot

        c = 'c_%s' % (plot)
        c = ROOT.TCanvas('c', 'c', 800, 800)
        if doLogY == True: c.SetLogy()
        if doLogX == True: c.SetLogx()

        if plot == 'higgsmass':
            h[param]['mh3'].Draw('same')
            h[param]['mdelta'].Draw('same')
            h[param]['mch'].Draw('same')
            h[param]['mA'].Draw('same')
            h[param]['mh2'].Draw('same')
            h[param]['mh1'].Draw('same')

            legend_mass.Draw()

        elif plot == 'decays':
            h[param]['dhw'].Draw()
            h[param]['dww'].Draw('same')
            h[param]['dee'].Draw('same')
            h[param]['duu'].Draw('same')
            h[param]['dtt'].Draw('same')
            h[param]['deu'].Draw('same')
            h[param]['det'].Draw('same')
            h[param]['dut'].Draw('same')
            
        tex.Draw()
        c.Update()
        c.SaveAs('/home/saolivap/WorkArea/Phenomenology/HTM/util/plots/%s_%s.png' % (plot,param))


