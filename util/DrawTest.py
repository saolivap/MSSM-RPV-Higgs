import ROOT 
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/AtlasStyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/AtlasStyle/AtlasLabels.C")
ROOT.gROOT.LoadMacro("/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/AtlasStyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad

#Flags
doLogX = False
doLogY = False

basepath = '/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/build/'
tree = 'mssmrpv'
hists = ['Mevar', 'Muvar', 'Mtvar', 'MeFrac', 'MuFrac', 'MtFrac']
params = ['lam121', 'lam131', 'lam231', 'lam122', 'lam132', 'lam232', 'lam123', 'lam133', 'lam233']
plots = ['Masses', 'Frac']
 
sample = basepath + 'mssmrpv.root'

outfile = ROOT.TFile('/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/hist.root','RECREATE')

# CREATING HISTOGRAMS
nbinsX = 1000
nbinsY = 10000
minbinX = -10
maxbinX = 10
minbinY = 0
maxbinY = 10

# 2-D Histograms
h = {}
for param in params:
    h[param] = {}

    for hist in hists:
        h[param][hist] = {}
        
        n = 'h_%s_%s' % (param, hist)
        h[param][hist] = ROOT.TH2F(n, n + "; %s [GeV]; %s [GeV]" % (hist, param), nbinsX, minbinX, maxbinX, nbinsY, minbinY, maxbinY)
        h[param][hist].SetDirectory(outfile)

# 1-D Histograms
h1 = {}
for param in params:
    h1[param] = {}
    n = 'h1_%s' % (param)
    h1[param] = ROOT.TH1F(n, n + "%s; %s" % (param, param), nbinsX, minbinX, maxbinX) 
    h1[param].SetDirectory(outfile)

h2 = {}
for hist in hists:
    h2[hist] = {}
    n = 'h2_%s' % (hist)
    h2[hist] = ROOT.TH1F(n, n + "%s; %s" % (hist, hist), nbinsX, minbinX, maxbinX) 
    h2[hist].SetDirectory(outfile)

# For Legend
g = {}
for hist in hists:
    g[hist] = {}
    n = 'h_%s' % (hist)
    g[hist] = ROOT.TH1F(n, n + "; %s [GeV]; Events" % (hist), 200, 0, 5000) 
    g[hist].SetDirectory(outfile)
    
# FILLING
f = ROOT.TFile.Open(sample)
t = f.Get(tree)
 
for param in params:
    print 'processing parameter: %s' % param

    n1 = 'htmp(%i,%i,%i)' % (nbinsX, minbinX, maxbinX) 
    if t.Draw('%s>>%s' % (param, n1), 
              '', 
              'goff'
    ):
        h1[param].Add(ROOT.gDirectory.Get('htmp'), 1.)

    
    for hist in hists:
        print 'processing histogram: %s' % hist
                    
        n = 'htmp(%i,%i,%i,%i,%i,%i)' % (nbinsX, minbinX, maxbinX, nbinsY, minbinY, maxbinY) 
        if t.Draw('%s:%s>>%s' % (hist, param, n), 
                  '', 
                  'goff'
        ):
            h[param][hist].Add(ROOT.gDirectory.Get('htmp'), 1.)

for hist in hists:
    n2 = 'htmp(%i,%i,%i)' % (nbinsX, minbinX, maxbinX) 
    if t.Draw('%s>>%s' % (hist, n2), 
              '', 
              'goff'
    ):
        h2[hist].Add(ROOT.gDirectory.Get('htmp'), 1.)


del f

# HIST, PARAM CONFIG
for hist in hists:
    for param in params:

        h[param][hist].SetMarkerSize(1)
        h[param][hist].SetMarkerStyle(ROOT.kDot)
        #for legend
        g[hist].SetMarkerSize(1)
        g[hist].SetMarkerStyle(ROOT.kFullCircle)

        if param == 'lam121': h[param][hist].GetXaxis().SetTitle('#lambda_{121}')
        elif param == 'lam122': h[param][hist].GetXaxis().SetTitle('#lambda_{122}')        
        elif param == 'lam123': h[param][hist].GetXaxis().SetTitle('#lambda_{123}')
        elif param == 'lam131': h[param][hist].GetXaxis().SetTitle('#lambda_{131}')
        elif param == 'lam132': h[param][hist].GetXaxis().SetTitle('#lambda_{132}')
        elif param == 'lam133': h[param][hist].GetXaxis().SetTitle('#lambda_{133}')
        elif param == 'lam231': h[param][hist].GetXaxis().SetTitle('#lambda_{231}')
        elif param == 'lam232': h[param][hist].GetXaxis().SetTitle('#lambda_{232}')
        elif param == 'lam233': h[param][hist].GetXaxis().SetTitle('#lambda_{233}')
        
        h[param][hist].GetYaxis().SetTitle('Masses')
        h[param][hist].GetXaxis().SetRangeUser(-2, 2)
        if param == 'lam122' or param == 'lam232': h[param][hist].GetXaxis().SetRangeUser(-0.4, 0.4)
        if hist == 'Mevar' or hist == 'Muvar' or hist == 'Mtvar': h[param][hist].GetYaxis().SetRangeUser(0.000001,10)
        elif hist == 'MeFrac' or hist == 'MuFrac' or hist == 'MtFrac': h[param][hist].GetYaxis().SetRangeUser(0.8,1.2)

        if hist == 'Mevar' or hist == 'MeFrac':
            h[param][hist].SetMarkerColor(ROOT.kRed)
            g[hist].SetMarkerColor(ROOT.kRed)
        if hist == 'Muvar' or hist == 'MuFrac':
            h[param][hist].SetMarkerColor(ROOT.kGreen)
            g[hist].SetMarkerColor(ROOT.kGreen)
        if hist == 'Mtvar' or hist == 'MtFrac':
            h[param][hist].SetMarkerColor(ROOT.kBlue)
            g[hist].SetMarkerColor(ROOT.kBlue)

for param in params:
    h1[param].GetXaxis().SetRangeUser(-2, 2)

for hist in hists:
    h2[hist].GetXaxis().SetRangeUser(0, 2)
    if hist == 'Mevar': h2[hist].GetXaxis().SetRangeUser(0, 0.05)
    if hist == 'Muvar': h2[hist].GetXaxis().SetRangeUser(0, 0.5)

# TEXT, LEGEND CONFIG
tex = ROOT.TLatex(0.2, 0.18, "Variation of all parameters")
tex.SetName('Test2')
tex.SetNDC(True)
tex.SetTextSize(0.04)

#legend configuration
xmin = 0.7
xmax = 0.85
ymin = 0.6
ymax = 0.85

for plot in plots:
    
    if plot == 'Masses':
        legend_mass = ROOT.TLegend(xmin, ymin, xmax, ymax) 
        legend_mass.SetFillColor(0)
        legend_mass.SetBorderSize(0)
        legend_mass.SetTextFont(42)
        legend_mass.SetTextSize(0.05) 
        legend_mass.SetName('turtle')
        legend_mass.SetShadowColor(0)

        legend_mass.AddEntry(g['Mevar'],'m_{e}', 'p')
        legend_mass.AddEntry(g['Muvar'],'m_{#mu}', 'p')
        legend_mass.AddEntry(g['Mtvar'],'m_{#tau}', 'p')

    elif plot == 'Frac':
        legend_frac = ROOT.TLegend(xmin, ymin, xmax, ymax+0.05) 
        legend_frac.SetFillColor(0)
        legend_frac.SetBorderSize(0)
        legend_frac.SetTextFont(42)
        legend_frac.SetTextSize(0.05) 
        legend_frac.SetName('turtle')
        legend_frac.SetShadowColor(0)

        legend_frac.AddEntry(g['MeFrac'],'#frac{m_{e,cal}}{m_{e,exp}}', 'p')
        legend_frac.AddEntry(g['MuFrac'],'#frac{m_{#mu,cal}}{m_{#mu,exp}}', 'p')
        legend_frac.AddEntry(g['MtFrac'],'#frac{m_{#tau,cal}}{m_{#tau,exp}}', 'p')

# PLOT IN CANVAS
for hist in hists:

    c2 = ROOT.TCanvas('c2', 'c2', 800, 800)
    h2[hist].Draw()       
    tex.Draw()    
    c2.Update()
    c2.SaveAs('/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/plots/%s.png' % (hist))

for param in params:

    c1 = ROOT.TCanvas('c1', 'c1', 800, 800)
    h1[param].Draw()       
    tex.Draw()    
    c1.Update()
    c1.SaveAs('/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/plots/%s.png' % (param))

    for plot in plots:
        print 'processing plot: %s' % plot
    
        c = 'c_%s' % (plot)
        c = ROOT.TCanvas('c', 'c', 800, 800)
        if doLogY == True: c.SetLogy()
        if doLogX == True: c.SetLogx()
        
        if plot == 'Masses':
            h[param]['Mevar'].Draw()
            h[param]['Muvar'].Draw('same')
            h[param]['Mtvar'].Draw('same')
            legend_mass.Draw()

        elif plot == 'Frac':
            h[param]['MeFrac'].Draw()
            h[param]['MuFrac'].Draw('same')
            h[param]['MtFrac'].Draw('same')
            legend_frac.Draw()

        tex.Draw()    
        c.Update()
        c.SaveAs('/home/saolivap/WorkArea/GitLab/MSSM-RPV-Higgs/util/plots/%s_%s.png' % (plot,param))


