// System include(s):
#include <iostream>
#include <iomanip>

// Local include(s):
#include "MSSMRpVModel.h"


MSSMRpVModel::MSSMRpVModel(Option option)
{
  
  std::cout << std::endl;
  std::cout << "[MSSMRpVModel :: MSSMRpVModel] Generating model option " << option;

  option_ = option;

  if ( option_ == ALL_FREE ) std::cout << ": Free variation of all parameters" <<std::endl;
  if ( option_ == ALL_FIXED ) std::cout << ": All parameters fixed" <<std::endl;
  if ( option_ == V1_FREE ) std::cout << ": All parameters fixed, v1 free" <<std::endl;

  debugtext_ = fopen("debug.txt", "w");  
}


MSSMRpVModel::~MSSMRpVModel()
{
  
  delete file;
  delete tree;
  
}
 
void MSSMRpVModel::initialize()
{
  
  mu = 0.0;
  Bmu = 0.0;
  m12 = 0.0;
  m22 = 0.0;
  
  vu = 0.0;
  vd = 0.0;
  
  lam121=0.0;
  lam131=0.0;
  lam231=0.0;
  lam211=0.0;
  lam311=0.0;
  lam321=0.0;
  lam122=0.0;
  lam132=0.0;
  lam232=0.0;
  lam212=0.0;
  lam312=0.0;
  lam322=0.0;
  lam123=0.0;
  lam133=0.0;
  lam233=0.0;
  lam213=0.0;
  lam313=0.0;
  lam323=0.0;
  lam121=0.0;
  lam122=0.0;
  
  e1=0.0;
  e2=0.0;
  e3=0.0;
  
  B1=0.0;
  B2=0.0;
  B3=0.0;
  
  Lam1=0.0;
  Lam2=0.0;
  Lam3=0.0;
  
  v1=0.0;
  v2=0.0;
  v3=0.0;
  
  tanbeta = 0.0;
  M2 = 0.0;
  ML1 = 0.0;
  ML2 = 0.0;
  ML3 = 0.0;
  beta = 0.0;
  alpha = 0.0;
  gw = 0;
  gz = 0;
  Mevar= 0.0;
  Muvar= 0.0;
  Mtvar= 0.0;
  MeFrac = 0.0;
  MuFrac = 0.0;
  MtFrac = 0.0;
  Mh0 = 0.0;
  Mh1 = 0.0;
  Mh2 = 0.0;
  Mh3 = 0.0;
  MA1 = 0.0;
  MA2 = 0.0;
  MA3 = 0.0;
  MH = 0.0;
  MA = 0.0;
  Xt = 0.0;
  Ms = 0.0;
  Htautau = 0.0;
  BRtau0 = 0.0;
  BRtau = 0.0;
  
  cpeven_mass_neg = 0;
  cpodd_mass_neg = 0;
  charged_mass_neg = 0;
  
}

void MSSMRpVModel::addBranches()
{

  file = new TFile("mssmrpv.root", "RECREATE");
  tree = new TTree("mssmrpv", "MSSMRpV Model");

  tree->Branch("v_{1}", &v1, "v1");
  tree->Branch("v_{2}", &v2, "v2");
  tree->Branch("v_{3}", &v3, "v3");
  tree->Branch("v_{u}", &vu, "vu");
  tree->Branch("v_{d}", &vd, "vd");
  tree->Branch("#mu", &mu, "mu");
  tree->Branch("m_{1}", &m12, "m1");
  tree->Branch("m_{2}", &m22, "m2");
  tree->Branch("B_{1}", &B1, "B1");
  tree->Branch("B_{#mu}", &Bmu, "Bmu");
  tree->Branch("#epsilon_{1}", &e1, "e1");
  tree->Branch("#epsilon_{2}", &e2, "e2");
  tree->Branch("#epsilon_{3}", &e3, "e3");
  tree->Branch("#lambda_{121}", &lam121, "lam121");
  tree->Branch("#lambda_{131}", &lam131, "lam131");
  tree->Branch("#lambda_{231}", &lam231, "lam231");
  tree->Branch("#lambda_{122}", &lam122, "lam122");
  tree->Branch("#lambda_{132}", &lam132, "lam132");
  tree->Branch("#lambda_{232}", &lam232, "lam232");
  tree->Branch("#lambda_{123}", &lam123, "lam123");
  tree->Branch("#lambda_{133}", &lam133, "lam133");
  tree->Branch("#lambda_{233}", &lam233, "lam233");
  tree->Branch("#Lambda_{1}", &Lam1, "Lam1");
  tree->Branch("tan(beta)", &tanbeta, "tanbeta");
  tree->Branch("beta", &beta, "beta");
  tree->Branch("alpha", &alpha, "alpha");
  tree->Branch("M_{2}", &M2, "M2");
  tree->Branch("M_{L1}", &ML1, "ML1");
  tree->Branch("M_{L2}", &ML2, "ML2");
  tree->Branch("M_{L3}", &ML3, "ML3");
  tree->Branch("m_{h1}", &Mh0, "mh1");
  tree->Branch("m_{h2}", &Mh2, "mh2");
  tree->Branch("m_{h3}", &Mh3, "mh3");
  tree->Branch("m_{A1}", &MA1, "mA1");
  tree->Branch("m_{A2}", &MA2, "mA2");
  tree->Branch("m_{A3}", &MA3, "mA3");
  tree->Branch("m_{A}", &MA, "mA");
  tree->Branch("m_{H}", &MA3, "mH");
  tree->Branch("m_{e}", &Mevar, "Mevar"); //en la matriz del charg
  tree->Branch("m_{u}", &Muvar, "Muvar");
  tree->Branch("m_{t}", &Mtvar, "Mtvar");
  tree->Branch("MeFrac", &MeFrac, "MeFrac");
  tree->Branch("MuFrac", &MuFrac, "MuFrac");
  tree->Branch("MtFrac", &MtFrac, "MtFrac");
  tree->Branch("D_{H#tau#tau}", &Htautau, "Htautau");
  tree->Branch("BRtau0", &BRtau0, "BRtau0");
  tree->Branch("BRtau", &BRtau, "BRtau");

}

void MSSMRpVModel::massMatrices()
{

  // FULL Chargino matrix
  TMatrixDSym Charged(5);
  
  Charged[0][0] = M2;
  Charged[0][1] = 1/sqrt(2)*gw*vu;
  Charged[0][2] = 0;
  Charged[0][3] = 0;
  Charged[0][4] = 0;  
  
  Charged[1][0] = 1/sqrt(2)*gw*vd;
  Charged[1][1] = mu;
  Charged[1][2] = -1/sqrt(2)*h1*v1;
  Charged[1][3] = -1/sqrt(2)*h2*v2;;
  Charged[1][4] = -1/sqrt(2)*h3*v3;  
  
  Charged[2][0] = 1/sqrt(2)*v1;
  Charged[2][1] = -e1;
  Charged[2][2] = 1/sqrt(2)*h1*vd + sqrt(2)*(lam211*v2+lam311*v3);
  Charged[2][3] = sqrt(2)*(lam212*v2+lam312*v3);
  Charged[2][4] = sqrt(2)*(lam213*v2+lam313*v3); 
  
  Charged[3][0] = 1/sqrt(2)*v2;
  Charged[3][1] = -e2;
  Charged[3][2] = sqrt(2)*(lam121*v1+lam321*v3);
  Charged[3][3] = 1/sqrt(2)*h2*vd + sqrt(2)*(lam122*v1+lam322*v3);
  Charged[3][4] = sqrt(2)*(lam123*v1+lam323*v3); 
  
  Charged[4][0] = 1/sqrt(2)*v3;
  Charged[4][1] = -e3;
  Charged[4][2] = sqrt(2)*(lam131*v1+lam231*v2);
  Charged[4][3] = sqrt(2)*(lam132*v1+lam232*v2);
  Charged[4][4] = 1/sqrt(2)*h3*vd + sqrt(2)*(lam133*v1+lam233*v2);

  // std:: cout << " ################ CHARGINO MATRIX ##################" << std::endl;
  // Charged.Print();
 	
  const TMatrixDSymEigen eigen_charged(Charged);
  const TVectorD eignValCharged = eigen_charged.GetEigenValues();
  const TMatrixD eignVecCharged = eigen_charged.GetEigenVectors();

   // Singular values and rotation matrices:
  TDecompSVD svd(Charged);
  svd.Decompose();
  TVectorD S = svd.GetSig();
  TMatrixD Aux1 = svd.GetU();
  TMatrixD Aux2 = svd.GetV();
  
  // std:: cout << " ## Singular Values ##" << std::endl;
  // S.Print();
  
  Mevar = S[4];
  Muvar = S[3];
  Mtvar = S[2];

  fprintf(debugtext_, " ## Lepton Masses ## \n");
  fprintf(debugtext_, "Electron mass Var = %f \n", Mevar);
  fprintf(debugtext_, "Muon mass Var = %f \n", Muvar);
  fprintf(debugtext_, "Tau mass Var = %f \n", Mtvar);
  fprintf(debugtext_, "\n");

  MeFrac = Mevar/Me;
  MuFrac = Muvar/Mu;
  MtFrac = Mtvar/Mt;

  fprintf(debugtext_, " ## Lepton Fraction; calculated and fixed mass ## \n");
  fprintf(debugtext_, "Electron fraction = %f \n", MeFrac);
  fprintf(debugtext_, "Muon fraction = %f \n", MuFrac);
  fprintf(debugtext_, "Tau fraction = %f \n", MtFrac);
  fprintf(debugtext_, "\n");
  
  //Checking that's diagonal: The svd are in the wrong order (FIX IT)
  const TMatrixD U(TMatrixD::kTransposed, Aux1); 
  const TMatrixD V(TMatrixD::kTransposed, Aux2);
  const TMatrixD Aux0( U, ::TMatrixD::kMult, Charged);
  const TMatrixD Diag0( Aux0, ::TMatrixD::kMult, Aux2);
        
  //Permutation Matrix (To fix the order of the EigenValues)
  TMatrixDSym Perm0(5);
 
  Perm0[0][0] = 1;
  Perm0[0][1] = 0;
  Perm0[0][2] = 0;
  Perm0[0][3] = 0;
  Perm0[0][4] = 0;
  		   
  Perm0[1][0] = 0;
  Perm0[1][1] = 1;
  Perm0[1][2] = 0;
  Perm0[1][3] = 0;
  Perm0[1][4] = 0;
  		   
  Perm0[2][0] = 0;
  Perm0[2][1] = 0;
  Perm0[2][2] = 0;
  Perm0[2][3] = 0;
  Perm0[2][4] = 1;
  		   
  Perm0[3][0] = 0;
  Perm0[3][1] = 0;
  Perm0[3][2] = 0;
  Perm0[3][3] = 1;
  Perm0[3][4] = 0;
  		   
  Perm0[4][0] = 0;
  Perm0[4][1] = 0;
  Perm0[4][2] = 1;
  Perm0[4][3] = 0;
  Perm0[4][4] = 0;
  
  const TMatrixD Unew( Perm0, TMatrixD::kMult, U);
  const TMatrixD Vnew( Perm0, TMatrixD::kMult, V);
  const TMatrixD VnewT( TMatrixD::kTransposed, Vnew);
  
  // std:: cout << " ## Rotation Matrices  ##" << std::endl;
  // Unew.Print();
  // Vnew.Print();

  const TMatrixD AUX0( Unew, TMatrixD::kMult, Charged);
  const TMatrixD ChargDiagNew( AUX0, TMatrixD::kMult,  VnewT);

  // std:: cout << " ## Diagonal Matrix ##" << std::endl;
  // ChargDiagNew.Print();
      
  // Full CP-Even Matrix:
  
  TMatrixDSym CPeven(5); 
  
  CPeven[0][0] =  pow(gz,2)*(3*pow(vd,2)-pow(vu,2)+ pow(v1,2) +pow(v2,2) +pow(v3,2) )/8 + m12 + pow(mu,2);
  CPeven[0][1] = -pow(gz,2)*vd*vu/4 - Bmu;
  CPeven[0][2] =  pow(gz,2)*vd*v1/4 - e1*mu ;
  CPeven[0][3] =  pow(gz,2)*vd*v2/4 - e2*mu ;
  CPeven[0][4] =  pow(gz,2)*vd*v3/4 - e3*mu ;

  CPeven[1][0] = -pow(gz,2)*vd*vu/4 - Bmu;
  CPeven[1][1] =  -pow(gz,2)*(-3*pow(vu,2) + pow(vd,2)+ pow(v1,2) +pow(v2,2) +pow(v3,2))/8 + m22 + pow(mu,2) + pow(e1,2) + pow(e2,2) +  pow(e3,2);
  CPeven[1][2] = -pow(gz,2)*vu*v1 + B1;
  CPeven[1][3] = -pow(gz,2)*vu*v2 + B2;
  CPeven[1][4] = -pow(gz,2)*vu*v3 + B3;
  
  CPeven[2][0] = pow(gz,2)*vd*v1/4 - e1*mu ;
  CPeven[2][1] = -pow(gz,2)*vu*v1 + B1 ;
  CPeven[2][2] = pow(e1,2) + pow(ML1,2) + pow(gz,2)*( pow(vd,2) - pow(vu,2) + 3*pow(v1,2) + pow(v2, 2) + pow(v3,2) )/8 ;
  CPeven[2][3] = 0;
  CPeven[2][4] = 0;
  
  CPeven[3][0] = pow(gz,2)*vd*v2/4 - e2*mu ;
  CPeven[3][1] = -pow(gz,2)*vu*v2 + B2 ;
  CPeven[3][2] = 0;
  CPeven[3][3] = pow(e2,2) + pow(ML2,2) + pow(gz,2)*( pow(vd,2) - pow(vu,2) + 3*pow(v2,2) + pow(v1, 2) + pow(v3,2) )/8 ;
  CPeven[3][4] = 0;
  
  CPeven[4][0] = pow(gz,2)*vd*v3/4 - e3*mu ;
  CPeven[4][1] = -pow(gz,2)*vu*v3 + B3 ;
  CPeven[4][2] = 0;
  CPeven[4][3] = 0;
  CPeven[4][4] =  pow(e3,2) + pow(ML3,2) + pow(gz,2)*( pow(vd,2) - pow(vu,2) + 3*pow(v3,2) + pow(v1, 2) + pow(v2,2) )/8 ;

  // std:: cout << " ################ CP EVEN MATRIX ##################" << std::endl;
  // CPeven.Print();
		
  const TMatrixDSymEigen eigen_even(CPeven);
  const TVectorD eignValEven = eigen_even.GetEigenValues();
  const TMatrixD eignVecEven = eigen_even.GetEigenVectors();
  
  // std:: cout << " ## EigenValues ##" << std::endl;
  // eignValEven.Print();
  
 // Permutation Matrix (To fix the order of the EigenValues)
  TMatrixDSym Perm(5); 
  
  Perm[0][0] = 0;
  Perm[0][1] = 0;
  Perm[0][2] = 0;
  Perm[0][3] = 1;
  Perm[0][4] = 0;
			   
  Perm[1][0] = 0;
  Perm[1][1] = 0;
  Perm[1][2] = 0;
  Perm[1][3] = 0;
  Perm[1][4] = 1;
			   
  Perm[2][0] = 1;
  Perm[2][1] = 0;
  Perm[2][2] = 0;
  Perm[2][3] = 0;
  Perm[2][4] = 0;
			   
  Perm[3][0] = 0;
  Perm[3][1] = 1;
  Perm[3][2] = 0;
  Perm[3][3] = 0;
  Perm[3][4] = 0;
			   
  Perm[4][0] = 0;
  Perm[4][1] = 0;
  Perm[4][2] = 1;
  Perm[4][3] = 0;
  Perm[4][4] = 0;

  const TMatrixD eignVecEvenT(TMatrixD::kTransposed, eignVecEven);  
  const TMatrixD PermT(TMatrixD::kTransposed, Perm);
  const TMatrixD IZQ(eignVecEven, TMatrixD::kMult, PermT );
  const TMatrixD DER(Perm, TMatrixD::kMult, eignVecEvenT);
  const TMatrixD AUX11( IZQ , TMatrixD::kMult, CPeven );
  const TMatrixD Diagofin(AUX11, TMatrixD::kMult, DER );
  
  const TMatrixD D(DER);  

  //  Diagofin.Print();
     
  // FULL CP-Odd matrix  
  TMatrixDSym CPodd(5);
  
  //CPodd[0][0] =   pow(gz,2)*(-pow(vu,2)+pow(vd,2)+pow(v1,2)+pow(v2,2)+pow(v3,2))/8+ m12 +pow(mu,2);
  CPodd[0][0] = Bmu*vu/vd + mu*(e1*v1 + e2*v2 + e3*v3)/vd;
  CPodd[0][1] =  Bmu;
  CPodd[0][2] = -mu*e1;
  CPodd[0][3] = -mu*e2;
  CPodd[0][4] = -mu*e3;
		 
  CPodd[1][0] =  Bmu;
  //CPodd[1][1] =  -pow(gz,2)*(-pow(vu,2) + pow(vd,2)+pow(v1,2)+pow(v2,2)+pow(v3,2))/8 + m22 + pow(mu,2) + pow(e1,2)+pow(e2,2)+pow(e3,2);
  CPodd[1][1] = Bmu*vd/vu - (B1*v1 + B2*v2 + B3*v3)/vu;
  CPodd[1][2] = -B1;
  CPodd[1][3] = -B2;
  CPodd[1][4] = -B3;
  
  CPodd[2][0] = -mu*e1;
  CPodd[2][1] = -B1;
  CPodd[2][2] = (2*e1*e1+2*pow(ML1,2))/2+pow(gz,2)*(-pow(vu,2)+pow(vd,2)+pow(v1,2)+pow(v2,2)+pow(v3,2))/8;
  CPodd[2][3] = 0;
  CPodd[2][4] = 0;
  
  CPodd[3][0] = -mu*e2;
  CPodd[3][1] = -B2;
  CPodd[3][2] = 0;
  CPodd[3][3] = (2*e2*e2+2*pow(ML2,2))/2+ pow(gz,2)*(-pow(vu,2)+pow(vd,2)+pow(v1,2)+pow(v2,2)+pow(v3,2))/8;
  CPodd[3][4] = 0;
  
  CPodd[4][0] = -mu*e3;
  CPodd[4][1] = -B3;
  CPodd[4][2] = 0;
  CPodd[4][3] = 0;
  CPodd[4][4] = (2*e3*e3+2*pow(ML3,2))/2+ pow(gz,2)*(-pow(vu,2)+pow(vd,2)+pow(v1,2)+pow(v2,2)+pow(v3,2))/8;

  // std:: cout << " ################ CP ODD MATRIX ##################" << std::endl;
  // CPodd.Print();
 
  const TMatrixDSymEigen eigen_odd(CPodd);
  const TVectorD eignValOdd = eigen_odd.GetEigenValues();
  const TMatrixD eignVecOdd = eigen_odd.GetEigenVectors();
 
  // std:: cout << " ## EigenValues ## " << std::endl;
  // eignValOdd.Print();
 
  // CP-Even Higgs masses - Mh1 (light SM Higgs-like), Mh2 and Mh3 (heavy CP-Even Higgs masses)
  if ( eignValEven[2] < 0 && eignValEven[1] < 0 && eignValEven[0] < 0 ) {
    ++cpeven_mass_neg;
  }
  else {
    Mh0 = sqrt(eignValEven[4]);
    Mh2 = sqrt(eignValEven[3]);
    Mh3 = sqrt(eignValEven[2]);
    Mh4 = sqrt(eignValEven[1]);
    Mh5 = sqrt(eignValEven[0]);
    //with quantum corrections we should obtain Mh1 >~ Mz ~ 125GeV
    Mh1 = sqrt( eignValEven[4] + deltaMh1 ); 

    fprintf(debugtext_, " ## CP-Even scalar masses ## \n");
    fprintf(debugtext_, "Light Higgs mass = %f \n", Mh0);
    fprintf(debugtext_, "Light Higgs mass w/corrections = %f \n", Mh1);    
    fprintf(debugtext_, "Heavy Higgs mass = %f \n", Mh2);
    fprintf(debugtext_, "sNeutrino1 mass = %f \n", Mh3);
    fprintf(debugtext_, "sNeutrino2 mass = %f \n", Mh4);
    fprintf(debugtext_, "sNeutrino3 mass = %f \n", Mh5);
    fprintf(debugtext_, "\n");
  
  }

  // CP-Odd Higgs masses 
  if ( eignValOdd[2] < 0 && eignValOdd[1] < 0 && eignValOdd[0] < 0 ) {
    ++cpeven_mass_neg;
  }
  else {
	  
    MA1 = sqrt(eignValOdd[4]);
    MA2 = sqrt(eignValOdd[3]);
    MA3 = sqrt(eignValOdd[2]);
    MA4 = sqrt(eignValOdd[1]);
    MA5 = sqrt(eignValOdd[0]);
    
    fprintf(debugtext_, " ## CP-Odd scalar masses ## \n");
    fprintf(debugtext_, "A1 mass = %f \n", MA1);
    fprintf(debugtext_, "A2 mass = %f \n", MA2);
    fprintf(debugtext_, "A3 mass = %f \n", MA3);    
    fprintf(debugtext_, "A4 mass = %f \n", MA4);    
    fprintf(debugtext_, "A5 mass = %f \n", MA5);
    fprintf(debugtext_, "\n");
    
  }
 
  OL441 = -gw*( V[3][0]*U[3][1]*D[0][0] + V[3][2]*U[3][0]*D[0][1] + V[3][0]*U[3][2]*D[0][2] + V[3][1]*U[3][1]*D[0][3] + V[3][0]*U[3][4]*D[0][4] )/sqrt(2)
    - ( h1*U[3][2]*V[3][2]*D[0][0]/sqrt(2)  + h2*U[3][3]*V[3][3]*D[0][0]/sqrt(2)   + h3*U[3][4]*V[3][4]*D[0][0]/sqrt(2) ) 
    + (h1*U[3][1]*V[3][2]*D[0][2]/sqrt(2) + h2*U[3][1]*V[3][3]*D[0][3]/sqrt(2)     + h3*U[3][1]*V[3][4]*D[0][4]/sqrt(2) ) 
    + sqrt(2)* (  lam121*U[3][3]*V[3][2]*D[0][2] + lam131*U[3][4]*V[3][2]*D[0][2] + lam231*U[3][4]*V[3][2]*D[0][3] 
		  +lam122*U[3][3]*V[3][3]*D[0][2] + lam132*U[3][4]*V[3][3]*D[0][2] + lam232*U[3][4]*V[3][3]*D[0][3]
		  +lam123*U[3][3]*V[3][4]*D[0][2] + lam133*U[3][4]*V[3][4]*D[0][2] + lam233*U[3][4]*V[3][4]*D[0][3] );
  
  fprintf(debugtext_, " ## Heavy Higgs coupling to muons ## \n");
  fprintf(debugtext_, "Exact coupling OL441 = %f \n", OL441);
  fprintf(debugtext_, "\n");

  OL551 = -gw*( Vnew[4][0]*Unew[4][1]*D[0][0] + Vnew[4][2]*Unew[4][0]*D[0][1] + Vnew[4][0]*Unew[4][2]*D[0][2] + Vnew[4][1]*Unew[4][1]*D[0][3] + Vnew[4][0]*Unew[4][4]*D[0][4] )/sqrt(2)
					    - (h1*Unew[4][2]*Vnew[4][2]*D[0][0]/sqrt(2)  + h2*Unew[4][3]*Vnew[4][3]*D[0][0]/sqrt(2)    + h3*Unew[4][4]*Vnew[4][4]*D[0][0]/sqrt(2) )
					    + (h1*Unew[4][1]*Vnew[4][2]*D[0][2]/sqrt(2)  + h2*Unew[4][1]*Vnew[4][3]*D[0][3]/sqrt(2)    + h3*Unew[4][1]*Vnew[4][4]*D[0][4]/sqrt(2) )
					    + sqrt(2)* ( lam121*Unew[4][3]*V[4][2]*D[0][2] + lam131*Unew[4][4]*V[4][2]*D[0][2] + lam231*Unew[4][4]*Vnew[4][2]*D[0][3] 
							 +lam122*Unew[4][3]*V[4][3]*D[0][2] + lam132*Unew[4][4]*V[4][3]*D[0][2] + lam232*Unew[4][4]*Vnew[4][3]*D[0][3]
							 +lam123*Unew[4][3]*V[4][4]*D[0][2] + lam133*Unew[4][4]*V[4][4]*D[0][2] + lam233*Unew[4][4]*Vnew[4][4]*D[0][3] );
				     
  fprintf(debugtext_, " ## Heavy Higgs coupling to taus ## \n");
  fprintf(debugtext_, "Exact coupling OL551 = %f \n", OL551);
  fprintf(debugtext_, "\n");
				     
  // Mixing angle alpha:
  
  alpha = 0.5* atan( ( pow(MA2,2) + pow(Mz,2) )*tan(2*beta) / (pow(MA2 , 2) - pow(Mz , 2)) );

  fprintf(debugtext_, " ## Checking decoupling limit parameters ## \n");
  fprintf(debugtext_, "cos (beta-alpha) = %f \n", cos(beta-alpha));
  fprintf(debugtext_, "sin (beta-alpha) = %f \n", sin(beta-alpha));
  fprintf(debugtext_, "\n");
  
}

void MSSMRpVModel::decayRate()
{

  // Coupling Exacto  (i,j) = 3 MUONES & (i,j) = 4 TAUS 
  // OLij1 = -gw*( V[i][0]*U[j][1]*D[0][0] + V[i][2]*U[j][0]*D[0][1] + V[i][0]*U[j][2]*D[0][2] + Vi1*U[j][1]*D[0][3] + V[i][0]*U[j][4]*D[0][4] )/sqrt(2)
  //   - (h1*U[j][2]*V[i][2]*D[0][0]/sqrt(2)  + h2*U[j][3]*V[i][3]*D[0][0]/sqrt(2)   + h3*U[j][4]*V[i][4]*D[0][0]/sqrt(2) )
  //   + (h1*U[j][1]*V[i][2]*D[0][2]/sqrt(2) + h2*U[j][1]*V[i][3]*D[0][3]/sqrt(2)   + h3*U[j][1]*V[i][4]*D[0][4]/sqrt(2) )
  //   + sqrt(2)* (  lam121*U[j][3]*V[i][2]*D[0][2] + lam131*U[j][4]*V[i][2]*D[0][2] + lam231*U[j][4]*V[i][2]*D[0][3] 
  // 		  +lam122*U[j][3]*V[i][3]*D[0][2] + lam132*U[j][4]*V[i][3]*D[0][2] + lam232*U[j][4]*V[i][3]*D[0][3]
  // 		  +lam123*U[j][3]*V[i][4]*D[0][2] + lam133*U[j][4]*V[i][4]*D[0][2] + lam233*U[j][4]*V[i][4]*D[0][3] );
  

  // Decay width H->tautau y otros para el Gamma total:
  Htautau = pow(OL551,2)*Mh2*pow( 1 - 4*pow(Mt,2)/pow(Mh2,2), 3.0/2.0) / (8*pi);
  
  Hmumu = pow(OL441,2)*Mh2*pow( 1 - 4*pow(Mt,2)/pow(Mh2,2), 3.0/2.0) / (8*pi);
  
  Htt = 3*pow(gw,2)*pow(Mtop,2)*pow(sin(alpha),2)*Mh2*pow( 1 - 4*pow(Mtop,2)/pow(Mh2,2), 3.0/2.0) / (32*pi*pow(Mw,2)*pow(sin(beta),2));
  
  Hbb = 3*pow(gw,2)*pow(Mbot,2)*pow(cos(alpha),2)*Mh2*pow( 1 - 4*pow(Mbot,2)/pow(Mh2,2), 3.0/2.0) / (32*pi*pow(Mw,2)*pow(cos(beta),2));
  
  HWW = pow(gw,2)*( pow(Mh2,4) - 4*pow(Mh2,2)*pow(Mw,2) + 12*pow(Mw,4) )*pow(cos(beta-alpha),2)*sqrt( 1 - 4*pow(Mw,2)/pow(Mh2,2) )/(64*pi*pow(Mw,2)*Mh2);
  
  HZZ = pow(gz,2)*( pow(Mh2,4) - 4*pow(Mh2,2)*pow(Mz,2) + 12*pow(Mz,4) )*pow(cos(beta-alpha),2)*sqrt( 1 - 4*pow(Mz,2)/pow(Mh2,2) )/(128*pi*pow(Mz,2)*Mh2);
  
  
  //Para comparar con los valores tipicos del MSSM Rp Conserving case:
  
  Htautau0 = pow(gw,2)*pow(Mt,2)*pow(cos(alpha),2)*Mh2*pow( 1 - 4*pow(Mt,2)/pow(Mh2,2), 3.0/2.0) / (32*pi*pow(Mw,2)*pow(cos(beta),2));
  
  Hmumu0 = pow(gw,2)*pow(Mu,2)*pow(cos(alpha),2)*Mh2*pow( 1 - 4*pow(Mt,2)/pow(Mh2,2), 3.0/2.0) / (32*pi*pow(Mw,2)*pow(cos(beta),2));

  // Width "total":
  total_width =  Htautau + Hmumu + Htt + Hbb + HWW + HZZ;  
  total_width0 = Htautau0 + Hmumu0 + Htt + Hbb + HWW + HZZ;

  fprintf(debugtext_, " ## Heavy Higgs Decay Rate ## \n");
  fprintf(debugtext_, "width( H -> tau tau ) = %f \n", Htautau);
  fprintf(debugtext_, "width( H -> mu mu ) = %f \n", Hmumu);
  fprintf(debugtext_, "width( H -> bb ) = %f \n", Hbb);
  fprintf(debugtext_, "width( H -> tt ) = %f \n", Htt);
  fprintf(debugtext_, "width( H -> WW ) = %f \n", HWW);
  fprintf(debugtext_, "width( H -> ZZ ) = %f \n", HZZ);
  fprintf(debugtext_, "width0( H -> tau tau ) = %f \n", Htautau0);
  fprintf(debugtext_, "width0( H -> mu mu ) = %f \n", Hmumu0);
  fprintf(debugtext_, "Total width = %f \n", total_width);
  fprintf(debugtext_, "\n");
 
  //Branching Ratios:
  
  BRtau0 = Htautau0/(total_width0);
  BRmu0 = Hmumu0/(total_width0);
  BRtau = Htautau/(total_width); 
  BRmu = Hmumu /(total_width); 
  BRWW = HWW/(total_width);   
  BRZZ = HZZ/(total_width) ;  
  BRbb = Hbb / (total_width);  
  BRtt = Htt / (total_width);

  fprintf(debugtext_, " ## Heavy Higgs Branching Ratio ## \n");
  fprintf(debugtext_, "BR( H -> tau tau ) = %f \n", BRtau);
  fprintf(debugtext_, "BR( H -> mu mu ) = %f \n", BRmu);
  fprintf(debugtext_, "BR( H -> bb ) = %f \n", BRbb);
  fprintf(debugtext_, "BR( H -> tt ) = %f \n", BRtt);
  fprintf(debugtext_, "BR( H -> WW ) = %f \n", BRWW);
  fprintf(debugtext_, "BR( H -> ZZ ) = %f \n", BRZZ);
  fprintf(debugtext_, "BR0( H -> tau tau ) = %f \n", BRtau0);
  fprintf(debugtext_, "BR0( H -> mu mu ) = %f \n", BRmu0);
  fprintf(debugtext_, "\n");

 
  // ############################ SM LIKE HIGGS ####################################
  
  //Just for checking with something we actually know, we calculate the BR of the SM like Higgs:

  float Mh0 = 126.1;
  
  thetaw = acos( gw/gz );
  
  float x = pow( Mw/Mh1, 2);
  float y = pow(Mz/Mh1, 2);
 
  Fx =  (1-8*x+20*x*x)/sqrt(4*x-1)*acos((3*x-1)/(2*sqrt(x*x*x)))-(1-x)/(6*x)*(2-13*x+47*x*x)-(1-6*x+4*x*x)*log(x)/2;
 
  Fy = (1-8*y+20*y*y)/sqrt(4*y-1)*acos((3*y-1)/(2*sqrt(y*y*y)))-(1-y)/(6*y)*(2-13*y+47*y*y)-(1-6*y+4*y*y)*log(y)/2;

  htautau = pow(gw,2)*pow(Mt,2)*pow(sin(alpha),2)*Mh1* pow( 1 - 4*pow(Mt,2)/pow(Mh1,2) , 3.0/2.0) / ( 32*pi*pow(Mw,2)*pow(cos(beta),2) );
  
  hmumu   = pow(gw,2)*pow(Mu,2)*pow(sin(alpha),2)*Mh1* pow( 1 - 4*pow(Mu,2)/pow(Mh1,2) , 3.0/2.0) / ( 32*pi*pow(Mw,2)*pow(cos(beta),2) );
  
  hbb     = 3*pow(gw,2)*pow(Mbot,2)*pow(sin(alpha),2)*Mh1* pow( 1 - 4*pow(Mbot,2)/pow(Mh1,2) , 3.0/2.0) / ( 32*pi*pow(Mw,2)*pow(cos(beta),2) );
 
  hZZ = 9*pow(gw,4)*Mh1*(7./12.-10./9.*sin(thetaw)*sin(thetaw)+40./27.*pow(sin(thetaw),4))*Fy / (512*pi*pi*pi*pow(cos(thetaw),4));

  hWW = 9*pow(gw,4)*Mh1 / (512*pi*pi*pi)*Fx;
 
  total_h = htautau + hmumu + hbb + hWW + hZZ;
 
  BRhtau= htautau/total_h;		 
  BRhmu = hmumu/total_h;	  
  BRhb  = hbb/total_h;	  
  BRhWW = hWW/total_h;	  
  BRhZZ = hZZ/total_h;

  fprintf(debugtext_, " ## Light Higgs Decay Rate ## \n");
  fprintf(debugtext_, "width( h -> tau tau ) (SM = 2.6e-04) = %f \n", htautau);
  fprintf(debugtext_, "width( h -> mu mu ) (SM = 9.0e-07) = %f \n", hmumu);
  fprintf(debugtext_, "width( h -> bb ) (SM = 2.4e-03) = %f \n", hbb);
  fprintf(debugtext_, "width( h -> WW ) (SM = 9.7e-04) = %f \n", hWW);
  fprintf(debugtext_, "width( h -> ZZ ) (SM = 1.22e-04) = %f \n", hZZ);
  fprintf(debugtext_, "Total width (SM Higgs at 125 GeV = 4.2 e-03) = %f \n", total_h);
  fprintf(debugtext_, "\n");

  fprintf(debugtext_, " ## Light Higgs Branching Ratio ## \n");
  fprintf(debugtext_, "BR( H -> tau tau ) (SM = 0.06192) = %f \n", BRhtau);
  fprintf(debugtext_, "BR( H -> mu mu ) (SM = 0.0002148) = %f \n", BRhmu);
  fprintf(debugtext_, "BR( H -> bb ) (SM = 0.5744) = %f \n", BRhb);
  fprintf(debugtext_, "BR( H -> WW ) (SM = 0.221) = %f \n", BRhWW);
  fprintf(debugtext_, "BR( H -> ZZ ) (SM = 0.02741) = %f \n", BRhZZ);
  fprintf(debugtext_, "\n");

  fprintf(debugtext_, " ## Angle fraction ## \n");
  fprintf(debugtext_, "sin(alpha)/cos(beta) = %f \n", sin(alpha)/cos(beta));
  fprintf(debugtext_, "\n");

}

void MSSMRpVModel::execute( int n_events )
{

  n_events_ = n_events;
  TRandom *rd = new TRandom();
  boost::progress_display show_progress( n_events_ );
  
  for (long int n_ev = 0; n_ev != n_events; n_ev++ )
    {

      fprintf(debugtext_, " ############### \n");
      fprintf(debugtext_, " ## EVENT N:%i ## \n", n_ev);
      fprintf(debugtext_, " ############### \n");
      fprintf(debugtext_, "\n");
      
      // Model parameters
      if ( option_ == ALL_FREE )
	{
	  // in the text, not in the script:
	  // Lambda_1, Lambda_2, m_l, M_A
	  // in the script, not in the text:
	  // vu, vd
	  
	  e1 = 0.5 * rd->Rndm(n_ev);
	  e2 = 0.5 * rd->Rndm(n_ev);
	  e3 = 0.5 * rd->Rndm(n_ev);
	  lam123 = 0.5 * rd->Rndm(n_ev);
	  lam133 = 0.5 * rd->Rndm(n_ev);
	  Lam1 = 0.5 * rd->Rndm(n_ev);
	 // m1 = 300 + 1700 * rd->Rndm(n_ev);
	 // m2 = 300 + 1700 * rd->Rndm(n_ev);
	  M2 = 400 + 2100 * rd->Rndm(n_ev);
	  tanbeta = 60 * rd->Rndm(n_ev);
	  mu = -2500 + 5000 * rd->Rndm(n_ev);
	  B1 = 500 + 2500 * rd->Rndm(n_ev);
	  MA = 200 + 2800 * rd->Rndm(n_ev);
	  ML1 = 2500 * rd->Rndm(n_ev);
	  v1 = pow(10,-5) * (rd->Rndm(n_ev));
	  v2 = pow(10,-5) * (rd->Rndm(n_ev));
	  v3 = pow(10,-5) * (rd->Rndm(n_ev));
	  
	}
      
      else if ( option_ == ALL_FIXED ) {
    
	//MSSM parameters:
	tanbeta = 20.0;
	mu = 600.0;
	M2 = 1000.0;
	ML1 = 1000.0;
	ML2 = 1000.0 ;
	ML3 = 1000.0 ;
	Bmu = 100000.0;
        
	//BRpV parameters: 
  	e1    = 0.5;
  	e2    = 0.5;
  	e3    = 0.5;
  	Lam1  = 0.5;
  	Lam2  = 0.5;
  	Lam3  = 0.5;
  	
	//TRpV
	// lam121 = 0.0* rd->Rndm(n_ev); // fijar a 0 (it affects Me)
  	// lam131 = 0.0*rd->Rndm(n_ev); // fijar a 0
  	// lam231 = 4.0*rd->Rndm(n_ev) - 2.0 ; // libre
			  
  	// lam122 = 0.8 * rd->Rndm(n_ev) - 0.4; // -0.4 to 0.4
  	// lam132 = 4.0*rd->Rndm(n_ev)  -2.0; // libre
  	// lam232 = 0.8 * rd->Rndm(n_ev) - 0.4 ; // -0.4 to 0.4
			  
  	// lam123 = 4.0*rd->Rndm(n_ev) - 2.0  ; //libre
  	// lam133 = 4.0*rd->Rndm(n_ev) - 2.0 ; // libre
  	// lam233 = 4.0*rd->Rndm(n_ev) - 2.0 ; //libre

	//lam121 = 0.0; // Me fucked
	lam121 = 4.0* rd->Rndm(n_ev) - 2.0; // fijar a 0 (it affects Me)
  	// lam131 = 4.0* rd->Rndm(n_ev) - 2.0; // fijar a 0
	lam131 = 0.0; // Me fucked
		
	lam231 = 0.0 ; // libre
	//lam231 = 4.0* rd->Rndm(n_ev) - 2.0; // Good
			  
  	lam122 = 0.0; // -0.4 to 0.4
  	lam132 = 0.0; // libre
  	lam232 = 0.0; // -0.4 to 0.4
			  
  	lam123 = 0.0; //libre
  	lam133 = 0.0; // libre
  	lam233 = 0.0; //libre

  	
	// Quantum Corrections Higgs Mass
  	Ms = 800;
  	Xt = 0.6*Ms;
  	  	
	}
      
      else if ( option_ == V1_FREE )
	{
	  v1 = 1000 * fabs(2 * (rd->Rndm(n_ev)) - 1);
	}
      
      fprintf(debugtext_, " ## Parameters ## \n");
      fprintf(debugtext_, "e1 = %f \n", e1);
      fprintf(debugtext_, "e2 = %f \n", e2);
      fprintf(debugtext_, "e3 = %f \n", e3);
      fprintf(debugtext_, "lam122 = %f \n", lam122);
      fprintf(debugtext_, "lam123 = %f \n", lam123);
      fprintf(debugtext_, "lam133 = %f \n", lam133  );
      fprintf(debugtext_, "Lam1 = %f \n", Lam1 );
      fprintf(debugtext_, "m1^2 = %f \n", m12 );
      fprintf(debugtext_, "m2^2 = %f \n", m22 );
      fprintf(debugtext_, "M2 = %f \n", M2 );
      fprintf(debugtext_, "tanbeta = %f \n", tanbeta );
      fprintf(debugtext_, "sin(2*beta) = %f \n", sin(2*beta) );
      fprintf(debugtext_, "mu = %f \n", mu );
      fprintf(debugtext_, "B1 = %f \n", B1 );
      fprintf(debugtext_, "beta = %f \n", beta );
      fprintf(debugtext_, "MA = %f \n", MA );
      fprintf(debugtext_, "ML = %f \n", ML1 );
      fprintf(debugtext_, "v1 = %f \n", v1 );
      fprintf(debugtext_, "v2 = %f \n", v2 );
      fprintf(debugtext_, "v3 = %f \n", v3 ); 
      fprintf(debugtext_, "\n");

      //Parametros en funcion de los imputs:	
      gw   = 2*Mw/246;
      gz   = 2*Mz/246;
      beta = atan(tanbeta);    
      vd   = 246*cos(beta);
      vu   = 246*sin(beta);  
      v1   = Lam1/mu - vd*e1/mu;
      v2   = Lam2/mu - vd*e2/mu;
      v3   = Lam3/mu - vd*e3/mu;
      Kappa= pow(246,2) - pow(v1,2) - pow(v2,2) - pow(v3,2) ;
     
      //Yukawas
      h1 = sqrt(2)*Me/vd;
      h2 = sqrt(2)*Mu/vd;
      h3 = sqrt(2)*Mt/vd;
      
      //Antisimetria de los lambda
      lam211= -lam121; 
      lam311= -lam131;
      lam321= -lam231;
      
      lam212= -lam122; 
      lam312= -lam132;
      lam322= -lam232;
      
      lam213= -lam123; 
      lam313= -lam133;
      lam323= -lam233;
		     
      //Correccion cuantica a la masa del higgs
      deltaMh1 = (3*pow(Mtop,4)*pow(gw,2)/( 8*pi*pi*Mw*Mw) )*( log( pow(Ms,2)/pow(Mtop,2) ) + pow(Xt,2)/pow(Ms,2)  - pow(Xt,4)/(12*pow(Ms,4) ) ) ;
            
      // Tadpole equation
	
      B1 = - pow(ML1,2)*v1/vu + mu*vd*e1/vu - e1*( v1*e1 + v2*e2 + v3*e3 )/vu - pow(gz,2)*v1*( pow(vd,2) - pow(vu,2) + pow(v1,2) + pow(v2,2) + pow(v3,2) )/(8*vu);
      B2 = - pow(ML2,2)*v2/vu + mu*vd*e2/vu - e2*( v1*e1 + v2*e2 + v3*e3 )/vu - pow(gz,2)*v2*( pow(vd,2) - pow(vu,2) + pow(v1,2) + pow(v2,2) + pow(v3,2) )/(8*vu);
      B3 = - pow(ML3,2)*v1/vu + mu*vd*e3/vu - e3*( v1*e1 + v2*e2 + v3*e3 )/vu - pow(gz,2)*v3*( pow(vd,2) - pow(vu,2) + pow(v1,2) + pow(v2,2) + pow(v3,2) )/(8*vu);
      
      m12 = -pow(mu,2) - pow(gz,2)*( pow(vd,2) - pow(vu,2) + pow(v1,2) + pow(v2,2) + pow(v3,2) )/8 + Bmu*vu/vd + mu*(v1*e1 + v2*e2 + v3*e3)/vd ;
      m22 = -pow(mu,2) + pow(gz,2)*( pow(vd,2) - pow(vu,2) + pow(v1,2) + pow(v2,2) + pow(v3,2) )/8 - ( pow(e1,2) + pow(e2,2) + pow(e3,2) ) + Bmu*vd/vu - (v1*B1 + v2*B2 + v3*B3)/vu  ;

      fprintf(debugtext_, " ## Useful variables ## \n");
      fprintf(debugtext_, "vu = %f \n", vu );
      fprintf(debugtext_, "vd = %f \n", vd );
      fprintf(debugtext_, "m1^2 = %f \n", m12 );
      fprintf(debugtext_, "m2^2 = %f \n", m22 );
      fprintf(debugtext_, "sin(2*beta) = %f \n", sin(2*beta) );
      fprintf(debugtext_, "B1 = %f \n", B1 );
      fprintf(debugtext_, "B2 = %f \n", B2 );
      fprintf(debugtext_, "B3 = %f \n", B3 );
      fprintf(debugtext_, "v1 = %f \n", v1 );
      fprintf(debugtext_, "v2 = %f \n", v2 );
      fprintf(debugtext_, "v3 = %f \n", v3 ); 
      fprintf(debugtext_, "Kappa = %f \n", Kappa ); 
      fprintf(debugtext_, "\n");
	
      // calculate masses of the Higgs bosons
      massMatrices();
      
      // calculate decays rates
      decayRate();
      
      // Fill all the branches with the parameters values
      tree->Fill();
      ++show_progress;

    }
  
  std::cout << "[MSSMRpVModel :: Finalize] Number of negative CP-Even Higgs masses: " << cpeven_mass_neg <<std::endl;
  std::cout << "[MSSMRpVModel :: Finalize] Number of negative CP-Odd Higgs masses: " << cpodd_mass_neg <<std::endl;
  std::cout << "[MSSMRpVModel :: Finalize] Number of negative Charged Higgs masses: " << charged_mass_neg <<std::endl;
  

  tree->Print();
  file->Write();
  fclose(debugtext_);
  
}



