// Dear emacs, this is -*- c++ -*-
#ifndef FULLEXAMPLE_MYLIBRARY_H
#define FULLEXAMPLE_MYLIBRARY_H

// Local include(s):
#include "enums.h"

// ROOT includes(s):
#include "TMath.h"
#include "TTree.h"
#include "TFile.h"
#include "TRandom.h"
#include "TMatrix.h"
#include "TVector.h"
#include "TMatrixD.h"
#include "TMatrixDSym.h"
#include "TMatrixDSymEigen.h"
#include "TDecompSVD.h"

// Boost include(s):
#include <boost/program_options.hpp>
#include <boost/progress.hpp>

// C++ libraries
//#include "stdio.h"

class MSSMRpVModel {

public:
  
  MSSMRpVModel(Option option);
  ~MSSMRpVModel();
  void addBranches();
  void initialize();
  void massMatrices();
  void decayRate();
  void execute(int n_events);

private:

  Option option_;
  int n_events_;
  TFile *file;
  TTree *tree;
  FILE *debugtext_;
 
  // model parameters
  Float_t mu;
  Float_t Bmu;
  Float_t m12; //higgs
  Float_t m22; //higgs
  Float_t vu; //vev
  Float_t vd; //vev
  Float_t tanbeta;
  Float_t beta;
  Float_t alpha;
  Float_t M2; //gauguino soft
  Float_t ML1; //slepton soft
  Float_t ML2;
  Float_t ML3;
  Float_t Mevar;
  Float_t Muvar;
  Float_t Mtvar;
  Float_t MeFrac;
  Float_t MuFrac;
  Float_t MtFrac;
  Float_t thetaw;
  Float_t Kappa;
  
  //Loop corrections
  Float_t Ms; 
  Float_t Xt;
  Float_t deltaMh1; 
   
  //RpV Bilinear Parameters
  Float_t e1;
  Float_t e2;
  Float_t e3;
  Float_t B1;
  Float_t B2;
  Float_t B3;
  Float_t Lam1;
  Float_t Lam2;
  Float_t Lam3;
  Float_t v1;
  Float_t v2;
  Float_t v3;
  
  //RpV  Trilinear Parameters
  Float_t lam121; 
  Float_t lam131;
  Float_t lam231; 
  Float_t lam211;
  Float_t lam311;
  Float_t lam321;
  Float_t lam122;
  Float_t lam132;
  Float_t lam232;
  Float_t lam212;
  Float_t lam312;
  Float_t lam322;
  Float_t lam123;
  Float_t lam133;
  Float_t lam233;
  Float_t lam213;
  Float_t lam313;
  Float_t lam323;

  // Physics constants
  float pi = TMath::Pi();
  float Mz = 91.1876;
  float Mw = 80.425;
  float Me = 0.511*0.001;
  float Mu = 105.7*0.001;
  float Mt = 1.777;
  float Mbot= 4.18;
  float Mtop= 172;
  float h1;
  float h2;
  float h3;
  //float fe = 2.9*0.000001, fu = 6.1*0.0001, ft = 1.0*0.01;
  float gw;
  float gz;

  // negative mass values counters
  int cpeven_mass_neg;
  int cpodd_mass_neg;
  int charged_mass_neg;

  // masses
  Float_t Mh0; //eigenvalue light higgs tree level
  Float_t Mh1; //eigenvalue light higgs + quantum corrections
  Float_t Mh2; 
  Float_t Mh3;
  Float_t Mh4;
  Float_t Mh5;
  Float_t MA1;
  Float_t MA2;
  Float_t MA3;
  Float_t MA4;
  Float_t MA5;
  Float_t MA;
  Float_t MH;
 
 // coupling CCH
  Float_t OL551;
  Float_t OL441;
  
  
 // decays
  Float_t Htautau0;
  Float_t Htautau;
  Float_t Hmumu;
  Float_t Hmumu0;
  Float_t Htt;
  Float_t Hbb;
  Float_t Hta;
  Float_t HWW;
  Float_t HZZ;
  
  Float_t htautau;
  Float_t hbb;    
  Float_t hmumu;  
  Float_t hWW;
  Float_t hZZ;
  
  //Width
  Float_t total_width;
  Float_t total_width0;
  
  Float_t total_h;
  Float_t Fx;
  Float_t Fy;
  
  //Branching
  Float_t BRmu;
  Float_t BRtau;
  Float_t BRtau0;
  Float_t BRmu0;
  Float_t BRtt;
  Float_t BRbb;
  Float_t BRZZ;
  Float_t BRWW;
  
  Float_t BRhtau;
  Float_t BRhmu ;
  Float_t BRhb  ;
  Float_t BRhWW ;
  Float_t BRhZZ ;
  

};

#endif
