// System include(s):
#include <iostream>

// Local include(s):
#include "MSSMRpVModel.h"

// ROOT include(s):
#include "TTree.h"

int main( int argc, char* argv[] ) {

  int events = 100000;

  boost::progress_timer t;  // start timing
  
  MSSMRpVModel *mssm = new MSSMRpVModel(ALL_FIXED);
  mssm->addBranches();
  mssm->execute(events);
  
  // Return gracefully:
  return 0;
}

