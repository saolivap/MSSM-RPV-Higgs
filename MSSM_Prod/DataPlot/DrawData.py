import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad

basepath = '/home/saolivap/WorkArea/Phenomenology/MSSM_Prod/DataPlot/dat/'
files = ['xsec', 'br_bb', 'br_tautau', 'br_mumu', 'br_WW', 'nev_run2_bb', 'nev_run2_tautau', 'nev_run2_mumu', 'nev_run2_WW', 'nev_run3_bb', 'nev_run3_tautau', 'nev_run3_mumu', 'nev_run3_WW', 'nev_run5_bb', 'nev_run5_tautau', 'nev_run5_mumu', 'nev_run5_WW']
plots = ['mg_test', 'mg_xsec', 'mg_br', 'mg_nev_run2', 'mg_nev_run3', 'mg_nev_run5']

p = {}
for iplot in plots:
    p[iplot] = {}
    p[iplot] = ROOT.TMultiGraph()

    g = {}
    for ifile in files:
        g[ifile] = {}
        name = '%s%s' % (basepath, ifile)
        g[ifile] = ROOT.TGraph( name + '.dat' )
        g[ifile].SetLineWidth(2)
        g[ifile].GetYaxis().SetTitleOffset(1.2)
        g[ifile].GetXaxis().SetTitleOffset(1.2)

        # line and marker style
        g[ifile].SetMarkerSize(2)
        g[ifile].SetLineWidth(1)
        g[ifile].SetLineStyle(7)        
        if ifile == 'xsec':
            g[ifile].SetMarkerStyle(ROOT.kOpenCircle)
        if ifile == 'br_bb' or ifile == 'nev_run2_bb' or ifile == 'nev_run3_bb' or ifile == 'nev_run5_bb':
            g[ifile].SetMarkerStyle(ROOT.kOpenCircle)
        if ifile == 'br_tautau' or ifile == 'nev_run2_tautau' or ifile == 'nev_run3_tautau' or ifile == 'nev_run5_tautau':
            g[ifile].SetMarkerStyle(ROOT.kOpenTriangleUp)
        if ifile == 'br_mumu' or ifile == 'nev_run2_mumu' or ifile == 'nev_run3_mumu' or ifile == 'nev_run5_mumu':
            g[ifile].SetMarkerStyle(ROOT.kOpenSquare)
        if ifile == 'br_WW' or ifile == 'nev_run2_WW' or ifile == 'nev_run3_WW' or ifile == 'nev_run5_WW':
            g[ifile].SetMarkerStyle(ROOT.kOpenDiamond)

        # line and market colour + legend catalog
        if ifile == 'xsec':
            g[ifile].SetMarkerColor(ROOT.kBlack)
            g[ifile].SetLineColor(ROOT.kBlack)
            xsec = g[ifile].Clone(ifile)            
        if ifile == 'br_bb' or ifile == 'nev_run2_bb' or ifile == 'nev_run3_bb' or ifile == 'nev_run5_bb':
            g[ifile].SetMarkerColor(ROOT.kRed)
            g[ifile].SetLineColor(ROOT.kRed)
            bb = g[ifile].Clone(ifile)
        if ifile == 'br_tautau' or ifile == 'nev_run2_tautau' or ifile == 'nev_run3_tautau' or ifile == 'nev_run5_tautau':
            g[ifile].SetMarkerColor(ROOT.kBlue)
            g[ifile].SetLineColor(ROOT.kBlue)
            tautau = g[ifile].Clone(ifile)
        if ifile == 'br_mumu' or ifile == 'nev_run2_mumu' or ifile == 'nev_run3_mumu' or ifile == 'nev_run5_mumu':
            g[ifile].SetMarkerColor(ROOT.kOrange)
            g[ifile].SetLineColor(ROOT.kOrange)
            mumu = g[ifile].Clone(ifile)
        if ifile == 'br_WW' or ifile == 'nev_run2_WW' or ifile == 'nev_run3_WW' or ifile == 'nev_run5_WW':
            g[ifile].SetMarkerColor(ROOT.kMagenta)
            g[ifile].SetLineColor(ROOT.kMagenta)
            WW = g[ifile].Clone(ifile)

        # graphs in each plot
        if iplot == 'mg_test':
            if ifile == 'xsec':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_xsec':
            if ifile == 'xsec':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_br':
            if ifile == 'br_bb' or ifile == 'br_tautau' or ifile == 'br_mumu' or ifile == 'br_WW':
                p[iplot].Add(g[ifile])
                
        if iplot == 'mg_nev_run2':
            if ifile == 'nev_run2_bb' or ifile == 'nev_run2_tautau' or ifile == 'nev_run2_mumu' or ifile == 'nev_run2_WW':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_nev_run3':
            if ifile == 'nev_run3_bb' or ifile == 'nev_run3_tautau' or ifile == 'nev_run3_mumu' or ifile == 'nev_run3_WW':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_nev_run5':
            if ifile == 'nev_run5_bb' or ifile == 'nev_run5_tautau' or ifile == 'nev_run5_mumu' or ifile == 'nev_run5_WW':
                p[iplot].Add(g[ifile])

        # legend configuration 
        xmin = 0.60
        xmax = 0.86
        ymin = 0.45
        ymax = 0.75

        legend = ROOT.TLegend(xmin, ymin, xmax, ymax)
        legend.SetFillColor(0)
        legend.SetBorderSize(0)
        legend.SetTextFont(42)
        legend.SetTextSize(0.03)
        legend.SetName('MSSM_Legend')
        legend.SetShadowColor(0)

        if iplot == 'mg_test':
            legend.AddEntry(xsec,'xsec', 'p')

        if iplot == 'mg_xsec':
            legend.AddEntry(xsec,'#sigma(gg->H)', 'p')

        if iplot == 'mg_br' or iplot == 'mg_nev_run2' or iplot == 'mg_nev_run3' or iplot == 'mg_nev_run5':
            legend.AddEntry(bb,'bb', 'p')
            legend.AddEntry(tautau,'#tau#tau', 'p')
            legend.AddEntry(mumu,'#mu#mu', 'p')
            legend.AddEntry(WW,'WW', 'p')

    # tex in canvas
    texATLAS = ROOT.TLatex(0.22, 0.2, "#sqrt{s}=14 TeV, tan#beta=60, #mu=200, mh_max") #was 0.63, 0.12
    texATLAS.SetName('latex1')
    texATLAS.SetNDC(True)
    texATLAS.SetTextSize(0.03)

    tex1 = ROOT.TLatex(0.67, 0.88, "") #was 0.63, 0.12
    if iplot == 'mg_nev_run2':
        tex1 = ROOT.TLatex(0.67, 0.88, "Lum = 36.1 fb^{-1}") #was 0.63, 0.12
    if iplot == 'mg_nev_run3':
        tex1 = ROOT.TLatex(0.67, 0.88, "Lum = 100 fb^{-1}") #was 0.63, 0.12
    if iplot == 'mg_nev_run5':
        tex1 = ROOT.TLatex(0.67, 0.88, "Lum = 3000 fb^{-1}") #was 0.63, 0.12
    tex1.SetName('latex1')
    tex1.SetNDC(True)
    tex1.SetTextSize(0.04)
    
    # canvas
#    p[iplot].SetMinimum(1)
#    p[iplot].SetMaximum(100)
    ct = ROOT.TCanvas('ct', 'ct', 800, 800)
    p[iplot].Draw('ALP')

    ct.SetLogy()
    if iplot == 'mg_xsec':
        p[iplot].GetYaxis().SetTitle('#sigma [fb]')
    if iplot == 'mg_br':
        p[iplot].GetYaxis().SetTitle('BR')
    if iplot == 'mg_nev_run2' or iplot == 'mg_nev_run3' or iplot == 'mg_nev_run5': 
        p[iplot].GetYaxis().SetTitle('N_events')
        
    p[iplot].GetXaxis().SetTitle('m_{H} [GeV]')
#    if iplot == 'mg_EffRate_Good':
#        p[iplot].GetXaxis().SetLimits(1,30)

    legend.Draw()
    texATLAS.Draw()
#    if iplot == 'mg_nev_run2':
    tex1.Draw()
    ct.Update()
    ct.SaveAs('/home/saolivap/WorkArea/Phenomenology/MSSM_Prod/DataPlot/plots/%s.pdf' % (iplot))




            
